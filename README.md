## Job list app

Simple list with sorting and smooth transition.

Vue3 + Composition API, Typescript

#### Project setup
```
yarn install
yarn serve
```
